/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package ru.tester;

public class Main {

    public static void main(String[] args) {
	// write your code here
//        class Dog{
//            String Color;
//
//            void something_no_difference(String color){
//                this.Color=color;
//            }
//
//        }
//        Dog myDog= new Dog(); ,
//        myDog.Color="Black";
//        System.out.println(myDog.Color);
//        myDog.something_no_difference("text");
//        System.out.println(myDog.Color);
//        byte a = 5 ^ 9;

        //00000101
        //00001001

        //00000001
//        byte x=5 & 1;
//
//        System.out.println(x);
//
//    byte b=0;
//    short a=128;
//    b=(byte)a;
//        System.out.println(a);

//        class Dog {
//            private String color;
//            private int size;
//            private int weight;
//            private void Gav(){
//                System.out.println("GAVGAVGAV");
//            }
//        }
//
//        class Spaniel extends Dog{
//
//            private boolean privivka;
//        }
//        Spaniel mySpaniel=new Spaniel();
//        mySpaniel.privivka=true;
//        System.out.println(mySpaniel.privivka);
//        Dog myDog = new Dog();

//        class Dog{
//            private int size;
//            private int weight;
//
//            public int getWeight() {
//                return weight;
//            }
//
//            public void setWeight(int weight) {
//                this.weight = weight;
//            }
//
//            public int getSize() {
//                return size;
//            }
//
//            public void setSize(int Size) {
//                this.size = Size;
//            }
//
//        }
//
//        class Labrador extends Dog{
//
//        }
//
//        class Spaniel extends Dog{
//
//        }
//        Labrador myLabrador = new Labrador();
//
//        Dog myDog=new Dog();
//        Spaniel mySpaniel = new Spaniel();
//        myDog.setSize(50);
//        mySpaniel.setSize(36);
//        System.out.println(mySpaniel.getSize());
//        System.out.println(myDog.getSize());
////        myDog.size=50;
////        myDog.setSize(50);
////        System.out.println(myDog.size);
////        System.out.println(myDog.getSize());

//        class Dog {
//            public Dog(int size, String color) {
//                this.size = size;
//                this.color = color;
//            }
//
//            public String getColor() {
//                return color;
//            }
//
//            public void setColor(String color) {
//                this.color = color;
//            }
//
//            public void setSize(int size) {
//                this.size = size;
//            }
//
//            int size;
//            String color;
//        }
        // конструкторы и наследование

//        class Dog{
//            int size;
//            int weight;
//            Dog(int s,int w) {
//                this.size=s;
//                this.weight=w;
//                System.out.println("надпись которая появл при создании экземпляра класса");
//            }
//
//        }
//        class Spaniel extends Dog {
//            Spaniel(int s, int w) {
//                super(s,w);
//            }
//        }
//        Dog myDog=new Dog(12,23);
//        System.out.println(myDog.size+" "+myDog.weight);
//        Spaniel mySpaniel = new Spaniel(50,22);
//        System.out.println(mySpaniel.size+" "+mySpaniel.weight);

        //в родительскую переменную типа privat передать что то из ребенка super.size=sizeSpaniel;
        //Super class
        //final защита класса от наследования
//        final class Dog {
//        }

//        class Dog {
//            public String color;
//            private final static int size=0;
//            private int weight;
//
//            public void Gav() {
//                System.out.println("gavgavgav: "+ size + ", weight: " + weight);
//            }
//        }
//
//        class Spaniel extends Dog {
//
//            boolean Privivka;
//            Spaniel(int sizeSpaniel, int weightSpaiel) {
////                super.size=sizeSpaniel;
//                super.weight=weightSpaiel;
//            }
//        }
//        Spaniel mySpaniel = new Spaniel(45,12);
//        mySpaniel.Gav();

        //работа с конструкторами
        class Dog{
           int size;
           int weight;
           int height;


           public Dog(int s){
               System.out.println("dog width s");
           }
           Dog(){
               System.out.println("dog");
           }
            Dog(int s,int w){
                this.size=s;
                this.weight=w;
                System.out.println("dog width s and w");
            }

        }
        class Spaniel extends Dog{
            public Spaniel(){
                System.out.println("spaniel");
            }
            Spaniel (int s){
                super(s);
                System.out.println("spaniel with s");
            }
            Spaniel(int s,int w){
                super(s,w);
                System.out.println("spaniel with s and w");
            }
            Spaniel(int s,int w, int h){
                super(s,w);
                System.out.println("spaniel with s and w2");
            }

        }
        Dog myDog = new Dog(10,4);
        Spaniel mySpaniel = new Spaniel(44,12,3);

    }
}
